/**
 * Created by Divine Touch on 1/2/15.
 */

angular.module('letchatonceApp').directive('focusOn', function($timeout) {
  link = function (scope, elem, attr) {
    scope.$on(attr.focusOn, function (e) {
      $timeout(function () {
        $('.focusMe').find('md-input-group').children(1).click();
      }, 0);
    });
  }

  return {
    restrict: 'A',
    replace:true,
    link: link
  }
})
