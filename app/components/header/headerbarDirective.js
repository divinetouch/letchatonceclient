/**
 * Created by Divine Touch on 12/30/14.
 */

angular.module('letchatonceApp').directive('headerBar',function(){
  return{
    restrict: 'E',
    templateUrl: '/components/header/headerBar.html',
    controller: 'headerBarCtrl'
  }
})
