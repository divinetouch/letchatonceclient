/**
 * Created by Divine Touch on 12/31/14.
 */

angular.module('letchatonceApp').controller('headerBarCtrl',function($scope,chatService){
  $scope.title = chatService.getChatRoomName();

  chatService.registerCallBack('toolBarCtrl',function(){
    $scope.title = "Chat Room: " + chatService.getChatRoomName().toUpperCase();
  });

})
