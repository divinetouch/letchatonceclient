'use strict';

/**
 * @ngdoc function
 * @name letchatonceApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the letchatonceApp
 */
angular.module('letchatonceApp')
  .controller('aboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
