/**
 * Created by Divine Touch on 1/7/15.
 */

angular.module('letchatonceApp').directive('tabBar',function(){
  return{
    restrict: 'E',
    controller: 'tabBarCtrl',
    templateUrl: '/components/tabs/tabBar.html'
  }
})
