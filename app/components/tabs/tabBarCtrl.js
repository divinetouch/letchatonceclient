/**
 * Created by Divine Touch on 1/7/15.
 */

angular.module('letchatonceApp').controller('tabBarCtrl', function ($scope, chatService, $timeout) {
  $scope.data = {
    selectedIndex: 0,
    thirdLabel: ""
  };

  chatService.registerCallBack('tabBarCtrl', function () {
      $scope.data.thirdLabel = chatService.getChatRoomName();
      $scope.data.selectedIndex = 2;
      $scope.thirdLocked = false;
  });

  $scope.next = function () {
    $scope.data.selectedIndex = Math.min($scope.data.selectedIndex + 1, 2);
  };
  $scope.previous = function () {
    $scope.data.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0);
  };
})
