'use strict';

/**
 * @ngdoc overview
 * @name letchatonceApp
 * @description
 * # letchatonceApp
 *
 * Main module of the application.
 */
angular
  .module('letchatonceApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMaterial'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/components/createChatRoom/createChatRoom.html',
        controller: 'createChatRoomCtrl'
      })
      .when('/join',{
        templateUrl: '/components/joinChatRoom/joinChatRoom.html',
        controller: 'joinChatRoomCtrl'
      })
      .when('/about', {
        templateUrl: '/components/about/about.html',
        controller: 'aboutCtrl'
      })
      .when('/chatRoom',{
        templateUrl: '/components/chatRoom/chatRoom.html',
        controller: 'chatRoomCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
