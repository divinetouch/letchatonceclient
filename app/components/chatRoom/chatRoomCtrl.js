/**
 * Created by Divine Touch on 12/31/14.
 */

angular.module('letchatonceApp').controller('chatRoomCtrl',function($scope, chatService, $location, $anchorScroll){

  $scope.messages = [];
  $scope.message;

  $scope.postMessage = function(){
    chatService.emit($scope.message, function(){
      console.log("message from client: " + $scope.message);
      $scope.message = "";
      $scope.gotoBottom();
      $scope.$broadcast('message-submitted');
      $scope.focus();
    });
    };

  $scope.focus = function(){
    $('.focusMe').find('md-input-group').children(1).click();
  }

  chatService.registerCallBack('chatRoomCtrl',function(){
    $scope.gotoBottom();
    $scope.$apply(function(){
      $scope.messages = chatService.getAllMessages;
    });
  });

  $scope.messages = chatService.getAllMessages;

  $scope.gotoBottom = function() {
    // set the location.hash to the id of
    // the element you wish to scroll to.
    $location.hash($scope.messages.length-1);

    // call $anchorScroll()
    $anchorScroll();
  };

});
