/**
 * Created by Divine Touch on 1/8/15.
 */

angular.module('letchatonceApp').directive('chatRoom',function(){
  return{
    restrict: 'E',
    controller: 'chatRoomCtrl',
    templateUrl: '/components/chatRoom/chatRoom.html'
  }
})
