/**
 * Created by Divine Touch on 12/30/14.
 */


angular.module('letchatonceApp').factory('backendService',function($q,$http,dateFilter){

  function makePost(chatInfo,action){
    var deferred = $q.defer();
    var url = 'http://localhost:3000/';
    var format = 'M/d/yy h:mm:ss a';

    var request = {
      method: 'POST',
      url: url + action,
      headers:{
        'Content-Type': 'application/json',
        'Accept':'application/json'
      },
      data:chatInfo
    }

    $http(request)
      .success(function(data){
        handleError(data,deferred);
      })
      .error(function(err){
        console.log(err);
        deferred.reject(err);
      });

    return deferred.promise;
  }

  function handleError(data,deferred){
    if(data.errorMessage !== undefined || data.error != undefined){
      console.log('data from server: ' + JSON.stringify(data));
      deferred.reject(data.errorMessage);
    }else{
      data.date=dateFilter(data.date,'M/d/yy h:mm:ss a');
      deferred.resolve(data);
    }

    return deferred.promise;
  }

  function createChatRoom(chatInfo){

    return makePost(chatInfo,'createChat');
  }

  function joinChatRoom(chatInfo){

    return makePost(chatInfo,'joinChat');

  }

  return{
    createChatRoom : createChatRoom,
    joinChatRoom: joinChatRoom
  }

})
