/**
 * Created by Divine Touch on 12/30/14.
 */

angular.module('letchatonceApp').factory('chatService',function(dateFilter){
  var socket = io.connect('http://localhost:3000/chat');
  var messages = [];
  var chatRoomName = "Let Chat Once";
  var callBacks = [];
  var format = 'M/d/yy h:mm:ss a';
  var displayName = undefined;

  function sendMessage(msg, callBack){
    var newMessage = {chatRoomName:getChatRoomName(), date: dateFilter(new Date(), format), displayName:displayName, message: msg};
    messages.push(newMessage);
    socket.emit('sendMessage', newMessage);
    if (callBack){
      callBack();
    }
  }

  function on(){
    socket.on(getChatRoomName(),function(data){
      console.log('message from server: ' + data.message);
      if(JSON.stringify(data) !== JSON.stringify(messages[messages.length-1])){
        messages.push(data);
        executeCallBack('chatroomctrl');
      }
    });

    socket.on('chat room close',function(data){
      if(JSON.stringify(data) !== JSON.stringify(messages[messages.length-1])){
        messages.pop();
        data.date = dateFilter(data.date,format);
        messages.push(data);
        executeCallBack('chatroomctrl');
      }
    });
  }

  function joinChatRoom(data){
    socket.emit('joinChat', data);
    executeCallBack('tabbarctrl');
    executeCallBack('chatroomctrl');
  }

  function chatRoomCreated(data){
    executeCallBack('chatroomctrl');
    executeCallBack('tabbarctrl');
  }

  function setChatRoomName(name){
    chatRoomName = name;
    executeCallBack('tabbarctrl');
  }

  function getChatRoomName(){
    return chatRoomName;
  }

  function socket(){
    return socket;
  }

  function addMessage(msg){
    messages.push(msg);
  }

  function registerCallBack(name,callback){
    callBacks.push({name: name, func: callback});
  }

  function setDisplayName(name){
    displayName = name;
  }

  function executeCallBack(funcName){
    var breakException = {};
    try{
      angular.forEach(callBacks, function(callback) {
        if (funcName === callback.name.toLowerCase()) {
          callback.func();
          throw breakException;
        }
      });
    }catch(e){
      if(e !== breakException){
        throw e;
      }
    }
  }

  return{
    emit: sendMessage,
    socket: socket,
    on: on,
    setChatRoomName : setChatRoomName,
    getChatRoomName : getChatRoomName,
    getAllMessages: messages,
    addMessage: addMessage,
    registerCallBack: registerCallBack,
    setDisplayName: setDisplayName,
    join:joinChatRoom,
    created: chatRoomCreated
  }

})
