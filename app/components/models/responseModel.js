/**
 * Created by Divine Touch on 1/3/15.
 */

angular.module('letchatonceApp').factory('responseModel', function () {

    var args = arguments.prototype.slice.call(arguments);
    var chatRoomName = undefined,
      chatRoomPassword = undefined,
      people = [],
      errorMessage = undefined;

    responseFunc = function () {
      chatRoomName = "";
    };

    responseFunc.chatRoomName = function (name) {
      chatRoomName = name;
      return responseFunc;
    }

    responseFunc.errorMessage = function (message) {
      errorMessage = message;
      return responseFunc;
    }

    return responseFunc;
})
