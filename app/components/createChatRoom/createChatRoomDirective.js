/**
 * Created by Divine Touch on 1/7/15.
 */

angular.module('letchatonceApp').directive('createChatRoom',function(){
  return{
    restrict: 'E',
    controller: 'createChatRoomCtrl',
    templateUrl: '/components/createChatRoom/createChatRoom.html'
  }
})
