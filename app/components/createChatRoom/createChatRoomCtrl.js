'use strict';

/**
 * @ngdoc function
 * @name letchatonceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the letchatonceApp
 */
angular.module('letchatonceApp')
  .controller('createChatRoomCtrl', function ($scope, backendService, $location,chatService, dateFilter) {
    $scope.chatInfo = {
      chatRoomName:"",
      displayName:"",
      duration: 0,
      maxParticipant: 0,
      password:""
    };

    $scope.formSubmit = function(){
      backendService.createChatRoom($scope.chatInfo).then(function(data){
        chatService.setChatRoomName(data.chatRoomName);
        chatService.setDisplayName(data.displayName);
        console.log(data);
        chatService.created(data);
        chatService.on();
      }, function (reason){
        alert(reason);
      })
    }
  });
