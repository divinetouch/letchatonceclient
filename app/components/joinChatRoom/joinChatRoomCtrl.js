'use strict';

/**
 * @ngdoc function
 * @name letchatonceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the letchatonceApp
 */
angular.module('letchatonceApp')
  .controller('joinChatRoomCtrl', function ($scope, backendService, $location,chatService) {
    $scope.chatInfo = {
      chatRoomName:"",
      displayName:"",
      duration: 0,
      password:""
    };

    $scope.formSubmit = function(){

      console.log($scope.chatInfo);

      backendService.joinChatRoom($scope.chatInfo).then(function(data){
        chatService.setChatRoomName(data.chatRoomName);
        chatService.setDisplayName(data.displayName);
        console.log(data);
        chatService.join(data);
        chatService.on();
      }, function(reason){
        alert(reason);
      })
    }
  });
