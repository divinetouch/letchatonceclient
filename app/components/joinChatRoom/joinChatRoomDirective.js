/**
 * Created by Divine Touch on 1/7/15.
 */

angular.module('letchatonceApp').directive('joinChatRoom',function(){
  return{
    restrict: 'E',
    controller: 'joinChatRoomCtrl',
    templateUrl: '/components/joinChatRoom/joinChatRoom.html'
  }
})
